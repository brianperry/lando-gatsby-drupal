# Starting Gatsby

## Gatsby new

When you start a new Gatsby project, the terminal output should look something
like this:

```
$ mkdir gatsby-test
$ cd gatsby-test
$ lando gatsby new
info Creating new site from git: https://github.com/gatsbyjs/gatsby-starter-default.git
Cloning into '/app/gatsby-test'...
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 1603 (delta 0), reused 1 (delta 0), pack-reused 1598
Receiving objects: 100% (1603/1603), 5.82 MiB | 8.58 MiB/s, done.
Resolving deltas: 100% (974/974), done.
success Created starter directory layout
info Installing packages...
✔ Which package manager would you like to use ? › yarn
info Preferred package manager set to "yarn"
yarn install v1.13.0
[1/4] Resolving packages...
[2/4] Fetching packages...
info fsevents@1.2.9: The platform "linux" is incompatible with this module.
info "fsevents@1.2.9" is an optional dependency and failed compatibility check. Excluding it from installation.
[3/4] Linking dependencies...
[4/4] Building fresh packages...
Done in 40.75s.
info Initialising git in /app/gatsby-test
Initialized empty Git repository in /app/gatsby-test/.git/
info Create initial git commit in /app/gatsby-test

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: empty ident name (for <(null)>) not allowed
error Command failed: git commit -m "Initial commit from gatsby: (https://github.com/gatsbyjs/gatsby-starter-default.git)"

*** Please tell me who you are.

Run

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

to set your account's default identity.
Omit --global to set the identity only in this repository.

fatal: empty ident name (for <(null)>) not allowed



  Error: Command failed: git commit -m "Initial commit from gatsby: (https://github.com/gatsbyjs/gatsby-starter-default.git)"
  *** Please tell me who you are.
  Run
    git config --global user.email "you@example.com"
    git config --global user.name "Your Name"
  to set your account's default identity.
  Omit --global to set the identity only in this repository.
  fatal: empty ident name (for <(null)>) not allowed

  - init-starter.js:126
    [lib]/[gatsby-cli]/lib/init-starter.js:126:5

  - Generator.next

  - next_tick.js:68 process._tickCallback
    internal/process/next_tick.js:68:7
```

Note that you are prompted to choose `npm` or `yarn` in this process.

It seems that the current version of Gatsby tries to start a Git repository and
fails because Git is not configured with a name and e-mail address. I will try
to do something about this, but for now you can just ignore this error.

## Gatsby develop

When you start up the Gatsby development server, you should see something like
this in your terminal:

```
$ lando gatsby develop --host=0.0.0.0
success open and validate gatsby-configs — 0.004 s
success load plugins — 0.291 s
success onPreInit — 0.005 s
success initialize cache — 0.006 s
success copy gatsby files — 0.014 s
success onPreBootstrap — 0.009 s
success source and transform nodes — 0.057 s
success building schema — 0.205 s
success createPages — 0.000 s
success createPagesStatefully — 0.034 s
success onPreExtractQueries — 0.001 s
success update schema — 0.018 s
success extract queries from components — 0.112 s
success run static queries — 0.126 s — 3/3 24.05 queries/second
success run page queries — 0.007 s — 5/5 759.58 queries/second
success write out page data — 0.013 s
success write out redirect data — 0.001 s

success Build manifest and related icons — 0.112 s
success onPostBootstrap — 0.113 s

info bootstrap finished - 3.035 s

 DONE  Compiled successfully in 2736ms                                                                                                                                   3:16:33 AM


You can now view gatsby-starter-default in the browser.

  Local:            http://0.0.0.0:8000/
  On Your Network:  http://192.168.176.3:8000/

View GraphiQL, an in-browser IDE, to explore your site's data and schema

  http://0.0.0.0:8000/___graphql

Note that the development build is not optimized.
To create a production build, use npm run build

info ℹ ｢wdm｣:
info ℹ ｢wdm｣: Compiled successfully.
```

The `nodejs` server will continue running until you kill it (e.g., by entering
CTRL-C in the same terminal session).

The numeric IP address does not seem to work.

## Build your Gatsby project

When you build your Gatsby site, you should see something like this in your
terminal:

```
$ lando gatsby build
success open and validate gatsby-configs — 0.005 s
success load plugins — 0.204 s
success onPreInit — 0.003 s
success delete html and css files from previous builds — 0.011 s
success initialize cache — 0.005 s
success copy gatsby files — 0.021 s
success onPreBootstrap — 0.008 s
success source and transform nodes — 0.050 s
success building schema — 0.184 s
success createPages — 0.000 s
success createPagesStatefully — 0.030 s
success onPreExtractQueries — 0.002 s
success update schema — 0.020 s
success extract queries from components — 0.087 s
success run static queries — 0.026 s — 3/3 121.30 queries/second
success run page queries — 0.004 s — 4/4 1259.77 queries/second
success write out page data — 0.002 s
success write out redirect data — 0.000 s
success Build manifest and related icons — 0.102 s
success onPostBootstrap — 0.103 s

info bootstrap finished - 2.195 s

success Building production JavaScript and CSS bundles — 5.591 s
success Building static HTML for pages — 0.578 s — 4/4 26.31 pages/second
info Done building in 8.366 sec
```

## Run your Gatsby project

When you start up the Gatsby server, you should see something like this in
your terminal:

```
$ lando gatsby serve --host 0.0.0.0 --port 8000
info gatsby serve running at: http://0.0.0.0:8000/
```

Use CTRL-C in your terminal window to stop the node server.
